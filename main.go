package main

import (
	"strconv"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

const MAX_PRIME = 300
const MIN_PRIME = 1

func main() {
	a := app.New()
	w := a.NewWindow("Hello")
	w.SetFixedSize(true)
	w.Resize(fyne.NewSize(400, 600))

	var c *fyne.Container
	var top *fyne.Container

	slider := widget.NewSlider(MIN_PRIME, MAX_PRIME)
	slider.OnChanged = func(f float64) {
		c = create_container(top, f)
		w.SetContent(c)
	}
	top = container.NewVBox(widget.NewRichTextFromMarkdown("**Number of Primes**"), slider)

	c = create_container(top, 1)
	w.SetContent(c)
	w.ShowAndRun()
}

func create_container(top *fyne.Container, f float64) *fyne.Container {
	cent := widget.NewRichTextFromMarkdown(primes(int(f)))
	cent_container := container.NewVScroll(cent)
	c := container.NewBorder(top, nil, nil, nil, cent_container)
	return c

}

func primes(N int) string {
	primes := prime_vals(N)

	prime_string := ""
	for i := range primes {
		prime_string += strconv.Itoa(primes[i]) + "\n\n"
	}

	return prime_string

}

func prime_vals(N int) []int {
	primes := []int{2, 3}

	prime_candidate := 3
	for len(primes) < N {
		prime_candidate++

		if is_prime(prime_candidate, primes) {
			primes = append(primes, prime_candidate)
		}
	}

	return primes
}

func is_prime(num int, primes []int) bool {
	// Assume the value is prime

	// Try to prove it non-prime
	for key, value := range primes {

		// Throw away the index value, we don't need it
		// lets call the current prime p
		_ = key
		p := value

		if value*value > num {
			// If sqrt can't be prime
			break
		}

		if is_divisible(num, p) {
			return false
		}
	}
	// If we couldn't find any divisors it must be prime
	return true

}

func is_divisible(num int, p int) bool {
	if num%p == 0 {
		return true
	} else {
		return false
	}
}
